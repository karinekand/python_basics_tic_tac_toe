# Print the board
game_board = [0, 1, 2, 3, 4, 5, 6, 7, 8]

def print_board():
    print('--------------')
    print('Current board: ')
    print(game_board[0:3])
    print(game_board[3:6])
    print(game_board[6:9])
    print('--------------')


# Take player input
def player_move(player_symbol):
    # Player_mark will be either 'X' or 'O'
    player_position = input(f"Please indicate the position to play as {player_symbol} : ")

    if (player_position == '0' or player_position == '1' or
            player_position == '2' or player_position == '3' or
            player_position == '4' or player_position == '5' or
            player_position == '6' or player_position == '7' or
            player_position == '8'):
        print('Valid choice!')
        player_position = int(player_position)

        if game_board[player_position] == 'X' or game_board[player_position] == 'O':
            print('Error: This position is already taken. Try again!')
            player_move(player_symbol)

        game_board.pop(player_position)
        game_board.insert(player_position, player_symbol)

    else:
        print('Error!')
        player_move(player_symbol)


print_board()
player_move(player_symbol='X')

# while True:
#   player_move(player_symbol='X')
# else:
#   player_move(player_symbol='O')

# if player_symbol == 'X':
#    player_symbol == 'O'
# else:
#    player_symbol == 'X'
# player_move(player_symbol)



print_board()
